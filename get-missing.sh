#!/bin/bash

years="2019"
base_url="https://files.pushshift.io/reddit"

for year in ${years}; do
    for month in {01..12}; do
        for dir in "comments" "submissions"; do
            # Test if we already have it.
            if [[ "${dir}" == "comments" ]]; then
                file_name="RC_${year}-${month}"
            else
                file_name="RS_${year}-${month}"
            fi
            hdfs dfs -ls /var/reddit/${file_name} > /dev/null 2>&1
            if (( $? != 0 )); then
                echo "Downloading from ${base_url}/${dir}/${file_name}.zst"
                ./update.sh "${base_url}/${dir}/${file_name}.zst"
            else
                echo "Skipping ${file_name}"
            fi
        done
        
    done
done

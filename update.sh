#!/bin/bash

set -e

check_parameters() {
    if [ "$#" -ne 1 ]; then
        echo "Illegal number of parameters"
        print_usage
        exit 1
    fi

    if [[ ( $1 == "--help") ||  ( $1 == "-h" ) ]]
    then
        print_usage
        exit 0
    fi
}

# Requires full URL as argument. Downloads from pushshift.io to 
# the stage directory and unzips.
fetch_file() {
    # Download file to locker. 8 minutes for 5 GB. 35m for 15 GB.
    cd ${stage_dir}
    time wget -c -nc ${1}

    # Unzip the zst file. 7 minutes for 60 GB. 19m for 160 GB.
    time unzstd $(basename ${1})
}

# Requires raw file name as arguments. Puts in HDFS. 10 minutes for 60 GB. 30m for 160 GB.
ingest_file() {
    time hdfs dfs -put ${1} ${dest_dir}
    hdfs dfs -chmod 644 ${dest_dir}${1}
    #hdfs dfs -chown hdfs ${dest_dir}${1} # Doesn't work.
}

# Requires raw file name as argument. Delete staged files from locker.
delete_file() {
    rm ${stage_dir}${1}
    rm ${stage_dir}${1}.zst
}

print_usage() {
    echo "Usage :"
    echo -e "      ./update.sh [ URL ]\n\n"
    echo "URL   : full url of reddit file including file name"
}

me=$(whoami)
stage_dir="/nfs/locker/arcts-cavium-hadoop-stage/home/${me}/"
dest_dir="/var/reddit/"

check_parameters $@
url=${1}
file_raw=$(basename -s .zst $1)

fetch_file $url
ingest_file $file_raw
delete_file $file_raw

Scripts to ingest Reddit dataset into HDFS from [pushshift.io](https://files.pushshift.io/reddit).

### Usage

Get any files through December 2019 that are not yet in HDFS.

```bash
# SSH to login server as ordinary user and clone repo.

# Download and ingest files.
./get-missing.sh
```

Alternatively, you can download and ingest a single file with `update.sh`.

```bash
# Or download and ingest a single file.
./update.sh URL

# For example:
./update.sh https://files.pushshift.io/reddit/comments/RC_2018-12.zst
```

### Data Notes
The Reddit comments data is usually delayed a few months. As of July 2020, there are no comments files available yet for 2020. When you are ready to download 2020 files, you'll need to edit a constant in `get-missing.sh` to update it to 2020.
